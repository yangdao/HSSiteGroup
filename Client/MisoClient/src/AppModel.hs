{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module AppModel(ViewModel(..),module  App.Article.Model) where
import Miso
import App.Article.Model
import AppEvent

data ViewModel = ViewModel {
  viewStack::[String],
  selectIdx::Int,
  articleModel::ArticleModel
} deriving (Eq)
