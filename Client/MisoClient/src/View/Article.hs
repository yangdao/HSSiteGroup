{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards,TemplateHaskell #-}
module View.Article (renderArticle,ArticleModel(..),initArticle) where
import Miso
import Miso.String hiding (map)
import Model.Article
import AppEvent
data ArticleModel = ArticleModel {
   article::[SimpleArticle]
} deriving (Show,Eq)

initArticle = ArticleModel []

renderArticle::ArticleModel -> View a
renderArticle am@ArticleModel{..} = div_ [class_ "scrollDiv"] ((map articleItem article) <> [div_ [class_ "clear"] []]) 
 where
   articleItem  SimpleArticle{..} = div_ [class_ "card"] [
      div_ [class_ "card-header"] [text $ toMisoString title],
      div_ [class_ "card-body"]   [text $ toMisoString context],
      div_ [class_ "card-footer"] [text $ toMisoString createTime]
    ]
