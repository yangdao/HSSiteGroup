{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Lib.View.Tabs(tabView,TabViewModel(..)) where
import Miso
import Miso.String (MisoString)
import Data.List

data TabViewModel action model rmodel = TabViewModel {
     tabWrapper::[View action] -> View action
    ,itemLst::[Int-> Bool -> View action]
    ,ctxList::[model -> View action]
    ,ctxModel::[rmodel -> model]
    ,rootWrapper::[View action] -> View action
}

tabView::rmd -> Int -> TabViewModel action md rmd -> View action
tabView inModel selIdx  TabViewModel{..} = rootWrapper [
    curCtx (curCtxFn inModel)
   ,tabWrapper (map (\(idx,f)->f idx (idx == selIdx)) tabItems)
  ]
 where
  curCtx = ctxList !! selIdx
  curCtxFn = ctxModel !! selIdx
  tabItems = zip [0..] itemLst


