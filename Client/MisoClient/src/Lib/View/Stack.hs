{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Lib.View.Stack where
import Miso
import Miso.String (MisoString)
import Data.List
import AppModel
import qualified Data.Map as M

stackView::[String] -> M.Map String (View a) -> View a
stackView viewKeyList viewMap =  div_ [class_ "stack_root"] viewList
 where
   viewList = map (\(idx,str) -> div_ [classList_ [("stack_elem",True),("stack_hide",idx /= 0)]] [viewMap M.! str]) zViewKeyList
   zViewKeyList = zip [0..] viewKeyList