{-# LANGUAGE JavaScriptFFI      #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE ForeignFunctionInterface #-}

module Lib.MD where

import qualified GHCJS.Types    as T
import qualified GHCJS.Foreign  as F

foreign import javascript unsafe "renderMD($1)" renderMD :: T.JSString -> IO T.JSString
{-
  window.onload = function()
  {
      var md = window.markdownit({html:true,xhtmlOut:true});
      let renderHTML = md.render($("#srcMD").val());
      $("#renderMD").html(renderHTML);
      $("pre code").each(function(i, block) {hljs.highlightBlock(block);});
  }
-}