module App.Article.Event(Event(..)) where
import App.Article.Model
import Data.Int
data Event =  RespArticle [SimpleArticle] 
              | OpenDetailArticle Int64 
              | RespDetailArticle DetailArticle
              | RefreshDetailArticle Int64
  deriving (Show,Eq)
