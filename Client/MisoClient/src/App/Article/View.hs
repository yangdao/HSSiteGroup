{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module App.Article.View where
import Miso
import Miso.String(MisoString,toMisoString)
import AppEvent
import App.Article.Model
import App.Article.Event
import Data.List
import Data.Maybe
import Data.Monoid
viewArticle::ArticleModel -> View Action
viewArticle am@ArticleModel{..} = div_ [class_ "app_context",textProp "style" "text-align:center"] [
     div_ [] viewArticleList]
 where
  viewArticleList = map viewArticleCard articleList


viewArticleCard::SimpleArticle -> View Action
viewArticleCard sa@SimpleArticle{..}= div_ [textProp "class" "card mcard",onClick $ ArticleEv $ OpenDetailArticle id] [div_ [class_ "card-header"] [
   div_ [textProp "class" "card-title h6"] [text $ toMisoString title]
 ],div_ [class_ "card-body"] [text $ toMisoString context]]


renderArticlePage::ArticleModel -> View Action
renderArticlePage am@ArticleModel{..} =  renderPage showArticle
  where 
   renderPage Nothing = div_ [textProp "style" "text-align:center"] [text "加载中..."]
   renderPage (Just dArticle@DetailArticle{..}) = div_ [] [
     div_ [textProp "class" "navbar myNavbar"] [
       section_ [] [div_ [textProp "class" "btn btn-primary btn-lg backBtn",onClick $ ArticleEv $ RefreshDetailArticle dId] 
                         [i_ [textProp "class" "icon icon-2x icon-refresh"] []]],
       section_ [textProp "class" "section-center title-center"] [h6_ [] [text $ toMisoString dTitle]],
       section_ [] [div_ [textProp "class" "btn btn-primary btn-lg backBtn",onClick $ BackEvent] 
                         [i_ [textProp "class" "icon icon-2x icon-back"] []]]
     ],
     div_ [textProp "style" "margin-bottom:6vh;overflow-y: hidden"] [
      iframe_ [textProp "srcdoc" (iframeHack <> (toMisoString dContext))] []
     ]
    ]

iframeHack = mconcat ["<style>body {padding:10px;}\r\n",
            "img {max-width:100%}",
            "</style><script language='javascript' src='static/highlight/highlight.pack.js'></script>",
            "<link rel='stylesheet' href='static/highlight/styles/vs.css'/>",
            "<link rel='stylesheet' href='static/bootstrap.min.css'/>",
            "<script language='javascript' src='static/jquery.min.js'></script>",
             "<script>",
             "window.onload = function() {",
               "$('pre code').each(function(i, block) {hljs.highlightBlock(block);});",
             "}",
             "</script>"]