{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module AppEvent(Action(..)) where
import Miso
import qualified App.Article.Event as AE


data Action = NoOp | DoSelectIdx Int 
                   | ArticleEv AE.Event
                   | BackEvent
              deriving (Show, Eq)