{-# LANGUAGE RecursiveDo,OverloadedStrings,ScopedTypeVariables,TemplateHaskell #-}
{-# LANGUAGE TemplateHaskell,GADTs,DeriveGeneric,FlexibleContexts,RankNTypes   #-}

module Control.TabsView where
import Reflex.Dom

data ViewEvent = Select Int | None

tabsView::(MonadWidget t m) => m (Event t a)
tabsView = do
  el "center" $ el "h1" $ text "TabsView"
  return  never