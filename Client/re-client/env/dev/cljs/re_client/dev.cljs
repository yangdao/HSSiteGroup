(ns ^:figwheel-no-load re-client.dev
  (:require
    [re-client.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
