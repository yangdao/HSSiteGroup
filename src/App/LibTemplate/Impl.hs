{-#LANGUAGE ScopedTypeVariables,FlexibleInstances,MultiParamTypeClasses #-}
module App.LibTemplate.Impl where
import qualified Text.Ginger as TG
import System.IO
import Control.Exception
import qualified Data.Text as DT
import Data.Functor.Identity
import App.Common.Table
import qualified Data.Aeson as DA
import qualified Data.Map as DM
import qualified Data.List as DL
import Data.Default (Default, def)

instance TG.ToGVal m v => TG.ToGVal m (DM.Map DT.Text v) where
    toGVal xs = helper (DM.map TG.toGVal xs)
       where
         helper :: DM.Map DT.Text (TG.GVal m) -> TG.GVal m
         helper xs =
             def
                 { TG.asHtml = mconcat . map TG.asHtml . DM.elems $ xs
                 , TG.asText = mconcat . map TG.asText . DM.elems $ xs
                 , TG.asBoolean = not . DM.null $ xs
                 , TG.isNull = False
                 , TG.asLookup = Just (`DM.lookup` xs)
                 , TG.asDictItems = Just $ DM.toList xs
                 }