{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
module App.UserCenter.TH.DBType where

import   Database.Persist
import   Database.Persist.Sqlite
import   Database.Persist.TH
import Data.Text

data SexType = Man | Woman deriving (Show,Read,Eq)
derivePersistField "SexType"