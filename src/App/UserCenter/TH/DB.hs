{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
module App.UserCenter.TH.DB where
import   Control.Monad.IO.Class
import   Database.Persist
import   Database.Persist.Sqlite
import   Database.Persist.TH
import Data.Text
import App.UserCenter.TH.DBType
import Data.Time


share [mkPersist sqlSettings, mkMigrate "migrateUser"] [persistLowerCase|
User
    account        Text 
    password       Text
    nickName       Text
    shortDesc      Text
    sex            SexType
    headImg        Text
    email          Text Maybe
    qq             Text Maybe
    phone          Text Maybe
    realName       Text Maybe
    idCard         Text Maybe
    roleType       Int
    balance        Int        --余额
    cumulative     Int        --累计充值
    registerTime   UTCTime    --注册时间
    UniqueAccount account
    deriving   Show
|]