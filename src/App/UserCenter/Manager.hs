{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE InstanceSigs,ScopedTypeVariables#-}
module App.UserCenter.Manager (queryUser) where
import qualified Data.Text as T
import App.Common.Types
import App.Common.Table
import App.UserCenter.DB
import Database.Persist
import Database.Persist.Sqlite
import Data.Int

queryUser::(WebAppM m) => PageInfo -> m (Table (Int64,User))
queryUser pi@(PageInfo cp ps) = do
 userList::[Entity User] <- runDB $ selectList [] [LimitTo ps,OffsetBy (cp * ps)]
 let retUser = map eu2tp userList
 return $ Table {total = length userList,pageInfo = pi,tableData = retUser}
 where
  eu2tp::Entity User -> (Int64,User)
  eu2tp (Entity k v) = (fromSqlKey  k,v)