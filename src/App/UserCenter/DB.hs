{-#LANGUAGE FlexibleInstances,InstanceSigs,OverloadedStrings#-}
module App.UserCenter.DB (module App.UserCenter.TH.DB,
                          module App.UserCenter.TH.DBType,defUser) where

import App.UserCenter.TH.DB
import App.UserCenter.TH.DBType
import Data.Default
import qualified Data.Time as DT
import Data.Text


defUser::IO User
defUser = do
   now <-  DT.getCurrentTime
   return $ User {
        userAccount = "",userPassword = "",userNickName = "",
        userHeadImg = "/static/img/defHead.jpeg",userSex = Man,userEmail = Nothing,
        userQq = Nothing,userPhone = Nothing,userRealName = Nothing,
        userRoleType = 0,userShortDesc = "这个人很懒什么都没写",
        userIdCard = Nothing,userBalance = 0,userCumulative = 0,userRegisterTime = now}
