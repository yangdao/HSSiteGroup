{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE ScopedTypeVariables#-}
{-# LANGUAGE FlexibleContexts#-}
{-# LANGUAGE QuasiQuotes     #-}
module App.Common.DataTypes (parseWebSafeText,convWebSafeText) where
import qualified Data.Text as T
import Control.Monad.Except
import Text.RawString.QQ

data WebSafeText = SafeText T.Text

isSafeText str = elem str ["<",">"]

parseWebSafeText::(MonadError T.Text m) => T.Text -> m WebSafeText
parseWebSafeText s = if isSafeText s 
                  then return $ SafeText s 
                  else throwError "不能包含特殊字符"

convWebSafeText::T.Text -> T.Text
convWebSafeText txt = conv replaceWord txt
 where
    replaceWord = [("\'","＇"),("\"","&quot;"),("<","&lt;"),(">","&gt;")]
    conv [] str             = str
    conv ((src,dst):xs) str = T.replace src dst (conv xs str)