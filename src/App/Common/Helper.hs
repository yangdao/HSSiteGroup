module App.Common.Helper(mapTuple,mapTuple3,toMD5)  where
import qualified Data.Text as T
import qualified Data.Digest.Pure.MD5 as MD5
import Data.String.Conversions

toMD5::T.Text -> T.Text
toMD5 str = cs.show.MD5.md5.cs $ str

mapTuple :: (a -> b) -> (a, a) -> (b, b)
mapTuple f (a1, a2) = (f a1, f a2)

mapTuple3 :: (a -> b) -> (a, a, a) -> (b, b, b)
mapTuple3 f (a1, a2, a3) = (f a1, f a2 ,f a3)