{-# LANGUAGE OverloadedStrings,ConstraintKinds,DeriveFunctor,InstanceSigs  #-}
module App.Common.Types (WebAppM(..),Session(..),SqlDB(..),ShowText(..)) where
import Data.Text
import Control.Monad.Trans.Reader (ReaderT)
import Control.Monad.IO.Unlift (MonadIO(..),MonadUnliftIO)
import Database.Persist.Sqlite

class (Monad m) => Session m where
 lookupSession::Text -> m (Maybe Text)
 deleteSession::Text -> m ()
 setSession::Text  -> Text -> m ()

class (MonadIO m) => SqlDB m where
    runDB::ReaderT SqlBackend m a -> m a

--Web应用需要使用Session,SqlDB和IO
class  (MonadIO m,Session m,SqlDB m) => WebAppM m

class ShowText a where
    showText::a -> Text