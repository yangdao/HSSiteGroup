{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies,DeriveGeneric               #-}
module App.SNSMessage.TH.DB where
import   Control.Monad.IO.Class
import   Database.Persist
import   Database.Persist.Sqlite
import   Database.Persist.TH
import Data.Text
import Data.Time
import Data.Int
import App.SNSMessage.TH.DBType
import GHC.Generics (Generic)


share [mkPersist sqlSettings, mkMigrate "migrateSNSMessage"] [persistLowerCase|
Message
    text              Text
    spoilerText       Text
    createTime        UTCTime
    accountId         Int64
    replyToId         Int64
    reblogOfId        Int64
    replyToAccountId  Int64
    visibility        MessageVisibility
    deriving Generic
    deriving Show
Comment
    context      Text
    ownerMsgId   Int64
    createTime   UTCTime
    ownerUserId  Int64
    deriving Show
Follow
    accountId         Int64
    targetAccountId   Int64
    createTime        UTCTime
    deriving Show
MediaAttachment
    mType           MediaType
    remoteUrl       Text
    accountId       Int64
    messageId       Int64
    UniqueMessageId messageId
    deriving Show
|]