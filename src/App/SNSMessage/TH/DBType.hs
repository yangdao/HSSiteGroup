{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies,DeriveGeneric #-}
module App.SNSMessage.TH.DBType where
import GHC.Generics (Generic)
import   Database.Persist
import   Database.Persist.Sqlite
import   Database.Persist.TH
import   Data.Aeson

data MessageVisibility = Hidden | Show deriving (Show,Read,Eq,Generic)
derivePersistField "MessageVisibility"

data MediaType = Img | Video | Audio deriving (Show,Read,Eq,Generic)
derivePersistField "MediaType"

instance ToJSON MessageVisibility
instance ToJSON MediaType