{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies,DeriveGeneric #-}
module App.Article.TH.DB where
import   Control.Monad.IO.Class
import   Database.Persist
import   Database.Persist.Sqlite
import   Database.Persist.TH
import Data.Text
import Data.Time
import Data.Int
import GHC.Generics

share [mkPersist sqlSettings, mkMigrate "migrateArticle"] [persistLowerCase|
ArticleCate
    name          Text
    parentId      Int64
    createTime    UTCTime
    deriving   Show Generic
Article
    title        Text
    context      Text
    ownerCateId  Int64
    createTime   UTCTime
    ownerUserId  Int64
    isPrivate    Bool
    deriving Show Generic
|]