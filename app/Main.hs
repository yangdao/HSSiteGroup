
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies,ScopedTypeVariables     #-}
{-# LANGUAGE InstanceSigs,UndecidableInstances    #-}
{-# LANGUAGE TypeSynonymInstances,FlexibleInstances#-}
module Main where
import           Yesod
import           Database.Persist.Sqlite
import           Control.Monad.Trans.Resource   ( runResourceT )
import           Control.Monad.Logger           ( runFileLoggingT )
import           Yesod.Static
import           App.UserCenter.DB
import           App.Article.DB
import           App.SNSMessage.DB
import           CMS.Main
import           CMS.Data
import qualified App.LibTemplate.LibTemplate   as TL
import qualified Data.List as L
import System.Environment
import           Control.Monad (when)

main2 :: IO ()
main2 = TL.testGinger

main :: IO ()
main =
  runFileLoggingT "log.txt"
    $ withSqlitePool "test.db3" 10
    $ \pool -> liftIO $ do
        runResourceT
          $ flip runSqlPool pool execMigration
        static@(Static settings) <- static "static"
        tpl                      <- TL.createTemplate $ TL.TemplateConfig
          { TL.rootPath    = "static/template/"
          , TL.isDebug     = True
          , TL.useSiteName = Just "default"
          }
        warp 3000 $ Master pool static Nothing--(Just tpl)


execMigration = do
  args <- liftIO getArgs
  let isMigration = L.elem "-migration" args
  when isMigration $ do
    runMigration migrateUser
    runMigration migrateArticle
    runMigration migrateSNSMessage
