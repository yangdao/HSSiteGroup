{-#LANGUAGE TemplateHaskell,QuasiQuotes,OverloadedStrings#-}
{-#LANGUAGE ScopedTypeVariables#-}
module CMS.Handler.Article where
import CMS.Data
import Yesod
import Data.Aeson
import qualified Data.Text as T
import App.Article.Manager
import App.Article.DB
import Control.Monad  
import App.Common.Table
import Data.Int

getAPIArticleListR::Int -> Handler Value
getAPIArticleListR curPage = do
  addHeader "Access-Control-Allow-Origin" "*"
  tbArticle <- queryArticle Nothing [] [] (PageInfo curPage 10)
  newList <- liftIO $ mapM dbArticle2S (tableData tbArticle)
  --let newTable = tbArticle {tableData = newList} 
  returnJson newList

getAPIArticleR::Int -> Handler Value
getAPIArticleR aid = do
  addHeader "Access-Control-Allow-Origin" "*"
  eArticle <- getArticle Nothing (fromIntegral aid)
  let eDArticle::Either T.Text (IO DetailArticle) = dbArticle2D <$> eArticle
  case eDArticle of
    Left l          -> returnJson (1::Int)
    Right ioArticle -> liftIO $ do
      article <- ioArticle
      returnJson article