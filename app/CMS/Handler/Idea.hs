{-# LANGUAGE OverloadedStrings               #-}
{-# LANGUAGE QuasiQuotes,ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell                 #-}
module CMS.Handler.Idea(getIdeaListR,postIdeaAddR,getIdeaHomeR) where
import CMS.Data
import Yesod
import CMS.View.Common
import CMS.View.Idea
import qualified App.UserCenter.Core as UC
import App.SNSMessage.Core
import App.SNSMessage.DB
import App.Common.Table
import Data.Int

ps = 10

getIdeaListR::Int -> Handler Value
getIdeaListR curPage = do
   mayLoginUserAndId <- UC.getLoginUserAndId
   let (userId,loginUser) = (fst <$> mayLoginUserAndId,snd <$> mayLoginUserAndId)
   tableData <- queryMessage userId (PageInfo curPage ps) []
   returnJson tableData


getIdeaHomeR::Handler Html
getIdeaHomeR = do
   mayLoginUserAndId <- UC.getLoginUserAndId
   let (userId,loginUser) = (fst <$> mayLoginUserAndId,snd <$> mayLoginUserAndId)
   tableData <- queryMessage userId (PageInfo 0 ps) []
   indexLayout (wIdeaHome loginUser tableData) loginUser


postIdeaAddR::Handler Html
postIdeaAddR = do
   loginUser <- UC.getLoginUser
   (context,reblogId,replyId) <- runInputPost $ (,,)
                                 <$> ireq textField "context"
                                 <*> ireq intField "reblogId"
                                 <*> ireq intField "replyId"
   ret <- postMessage context reblogId replyId
   case ret of
      Left  l -> jumpMessage l (IdeaR IdeaHomeR) loginUser
      Right _ -> jumpMessage "发布成功" (IdeaR IdeaHomeR) loginUser