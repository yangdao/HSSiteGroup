{-# LANGUAGE OverloadedStrings,InstanceSigs,TupleSections    #-}
{-# LANGUAGE QuasiQuotes,MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell,TypeFamilies,FlexibleInstances #-}
module CMS.Handler.Admin.AdminHome where
import Yesod
import CMS.Data
import CMS.View.Admin.Common

getAdminHomeR::Handler Html
getAdminHomeR = adminLayout ttt Nothing
 where
   ttt = [whamlet|
   <div .row>
    <div .col-lg-12>
     <h1 .page-header>首页
   |]