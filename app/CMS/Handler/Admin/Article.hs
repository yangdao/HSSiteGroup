{-# LANGUAGE OverloadedStrings,InstanceSigs,TupleSections    #-}
{-# LANGUAGE QuasiQuotes,MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell,TypeFamilies,FlexibleInstances #-}
module CMS.Handler.Admin.Article (getAdminArticleCateR,getAdminArticleListR,postAdminArticleDelR,
postAdminArticleCateAddR,getAdminArticleAddR,postAdminArticleCateDelR,
postAdminArticleAddR,getAdminArticleEditorR,postAdminArticleEditorR) where
import Yesod
import CMS.Data
import CMS.View.Admin.Common
import App.Article.Manager
import App.Common.Table
import CMS.View.Admin.Article
import Data.Int
import Data.Text
import qualified App.UserCenter.Core as UC
import Data.Maybe
import App.Article.DB hiding (SimpleArticle(..))

getAdminArticleCateR::Handler Html
getAdminArticleCateR = do
  cateList <- getAllCate
  cateNode    <- getCateNode
  adminLayout  (wCateList cateList cateNode)  Nothing

getAdminArticleListR::Int64 -> Handler Html
getAdminArticleListR pageNum = do
    mayLoginId <- UC.getLoginUserId
    queryArticle <- queryArticle mayLoginId [] [Desc ArticleCreateTime] $ PageInfo (fromIntegral pageNum) 15
    adminLayout (wArticleList queryArticle) Nothing

getAdminArticleAddR::Handler Html
getAdminArticleAddR = do
    cateList <- getAllCate
    adminLayout (wAddArticle cateList) Nothing

postAdminArticleCateAddR::Handler Html
postAdminArticleCateAddR =  do
  (cateName,pid) <- runInputPost $ (,) <$> ireq textField "cateName"
                                       <*> ireq intField "pid"
  retNew <- newCate cateName pid
  case retNew of
    Left  l -> adminJump l (AdminR AdminArticleCateR)
    Right r -> redirect $  AdminR AdminArticleCateR


postAdminArticleCateDelR::Handler Html
postAdminArticleCateDelR = do
  delId <- runInputPost $ id <$> ireq intField "delId"
  ret <- deleteCate delId
  case ret of
    Left l  -> adminJump l (AdminR AdminArticleCateR)
    Right _ -> redirect $ AdminR AdminArticleCateR

getArticlePost::Handler (Text,Text,Int64,Maybe Bool)
getArticlePost = runInputPost $ (,,,) <$> ireq textField "title"
  <*> ireq textField "context"
  <*> ireq intField "pid"
  <*> iopt boolField "isPrivate"

postAdminArticleAddR::Handler Html
postAdminArticleAddR = do
    mayLoginId <- UC.getLoginUserId
    let userId = fromMaybe 0 mayLoginId
    (title,context,pid,mayIsPrivate) <- getArticlePost
    let isPrivate = fromMaybe False  mayIsPrivate
    ret <- addArticle title context pid userId isPrivate
    case ret of
        Left l  -> adminJump l (AdminR AdminArticleAddR)
        Right r -> redirect $  AdminR $ AdminArticleListR 0

postAdminArticleDelR::Handler Html
postAdminArticleDelR = do
    delId <- runInputPost $ id <$> ireq intField "delId"
    deleteArticle delId
    adminJump "删除成功" (AdminR $ AdminArticleListR 0)

getAdminArticleEditorR::Int64 -> Handler Html
getAdminArticleEditorR editId = do
    mayUserId <- UC.getLoginUserId
    eArticle <- getArticle mayUserId editId
    cateList <- getAllCate
    case eArticle of
        Left  l -> adminJump l (AdminR $ AdminArticleEditorR editId)
        Right r -> adminLayout (wEditorArticle r cateList) Nothing

postAdminArticleEditorR::Int64 -> Handler Html
postAdminArticleEditorR editId = do
     (title,context,pid,mayIsPrivate) <- getArticlePost
     let isPrivate = fromMaybe False  mayIsPrivate
     ret <- updateArticle editId title context pid isPrivate
     liftIO $ putStrLn $ show isPrivate
     case ret of
       Left  l -> adminJump l (AdminR $ AdminArticleEditorR editId)
       Right r -> adminJump "修改成功" (AdminR $ AdminArticleEditorR editId)