{-# LANGUAGE OverloadedStrings,InstanceSigs,TupleSections    #-}
{-# LANGUAGE QuasiQuotes,MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell,TypeFamilies,FlexibleInstances #-}
module CMS.Handler.Admin.User where
import Yesod
import CMS.Data
import CMS.View.Admin.Common
import App.UserCenter.Manager
import App.Common.Table
import App.UserCenter.DB
import Data.Maybe
getAdminUserListR::Handler Html
getAdminUserListR = do
  userTable <- queryUser $ PageInfo 0 10
  adminLayout [whamlet|
<div .row>
 <div .col-lg-12>
  <h1 .page-header>用户列表
  <div>
   <table .table .table-striped .table-bordered .table-hover .dataTable>
     <thead>
       <tr>
        <th width="100px">ID
        <th width="120px">账号
        <th width="120px">昵称
        <th>邮箱
        <th>手机
        <th>QQ
        <th>余额
        <th width="100px">权限级别
        <th width="150px">操作
     <tbody>
      $forall (key,value) <- (tableData userTable)
       <tr>
        <td>#{key}
        <td>#{userAccount value}
        <td>#{userNickName value}
        <td>#{fromMaybe "" $ userEmail value}
        <td>#{fromMaybe "" $ userPhone value}
        <td>#{fromMaybe "" $ userQq value}
        <td>#{userBalance value}
        <td>#{userRoleType value}
        <td>
         &nbsp;&nbsp;&nbsp;&nbsp;
         <a  .btn-link href="#">删除
         <span>&nbsp;&nbsp;&nbsp;&nbsp;
         <a  .btn-link href="#">编辑
|] Nothing