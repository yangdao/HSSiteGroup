{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell,RecordWildCards #-}
module CMS.View.Idea(wIdeaHome) where
import CMS.Data
import CMS.View.Common
import App.Common.Table
import App.SNSMessage.DB
import Yesod
import App.UserCenter.DB
import Data.Int

wMsgCard::Message -> Widget
wMsgCard msg@Message{..} = [whamlet|
  <div .ideaCard>
    <div .ideaCardCInnerContext>
      <div .ideaFace>
        <img src="/static/upfile/user/yangdao/head.jpeg">
      <div .ideaContext>
        <div .ideaUserInfo>
          <a href="#">羊刀怎么合
          <div .ideaTime>9102年1月9日 来自 火星
        <div .ideaInnerText>#{messageSpoilerText}
      <div .clear>
      <ul .buttomBar>
          <li><a href="#"><i class="fa fa-thumbs-o-up fa-lg"></i>0</a>
          <li><a href="#"><i class="fa fa-commenting-o fa-lg"></i>0</a>
          <li><a href="#"><i class="fa fa-mail-reply fa-lg"></i>0</a>     
|]

wIdeaHome::Maybe User -> Table (Int64,Message) -> Widget
wIdeaHome mayLogin msgTable@Table{..} = do
  setTitle "Idea"
  addScriptRemote     "/static/Admin/vendor/jquery/jquery.min.js"
  addScriptRemote     "/static/jquery.textarea_autosize.js"
  addStylesheetRemote "/static/Admin/vendor/font-awesome/css/font-awesome.min.css"
  [whamlet|
    <div .row #index_context>
      <div .col-md-9 #left_layout>
       <div .ideaSendCard>
        <form form style="text-align:center;margin:auto" method="POST"  action="@{IdeaR IdeaAddR}">
         <input type="hidden" name="reblogId" value="-1">
         <input type="hidden" name="replyId" value="-1">
         <div .sendIdea #sendDiv>
          <textarea #sendTextArea spellcheck="false"  name="context"  rows="1" >
         <div .sendBar>
          <button type="submit" .sendBtn .btn .btn-primary>发送
       $forall (id,msg) <- tableData   
        ^{wMsgCard msg}
       $if (isNext msgTable)
        <div #loadMore .ideaCard .loadMore>加载更多
      <div .col-md-3 #right_layout>
        ^{wUserCard  mayLogin}
        ^{wFriendLink}
  |]
  toWidget [julius|
   var pageIndex = 0;
   $(function () 
     {
       $("#sendTextArea").focus(function(){
         $("#sendDiv").addClass("sendDivFocus");
       });
       $("#sendTextArea").blur(function(){
        $("#sendDiv").removeClass("sendDivFocus");
       });
       $("#sendTextArea").textareaAutoSize();
       $("#loadMore").click(loadMore);
       
     });
   function loadMore()
   {
     $.get("/idea/list/"+(pageIndex+1),function(data,status)
     {
       pageIndex++;
       if((data.pageInfo.curPage+1) * data.pageInfo.pageSize >= data.total)
       {
          $("#loadMore").hide();
       }
       for (i=0;i<data.tableData.length;i++)
       {
          [curId,curIdea] = data.tableData[i];
          $("#loadMore").before('<div class="ideaCard">\
             <div class="ideaCardCInnerContext">\
               <div class="ideaFace">\
                 <img src="/static/upfile/user/yangdao/head.jpeg" />\
               </div>\
               <div class="ideaContext">\
                  <div class="ideaUserInfo">\
                      <a href="">羊刀怎么合</a>\
                      <div class="ideaTime">9102年1月9日 来自 火星\
                  </div>\
                  <div class="ideaInnerText">'+curIdea.messageSpoilerText+'</div>\
               </div>\
             </div>\
             <div class="clear" />\
               <ul class="buttomBar">\
                <li><a href="#"><i class="fa fa-thumbs-o-up fa-lg"></i>0</a>\
                <li><a href="#"> <i class="fa fa-commenting-o fa-lg"></i>0</a>\
                <li><a href="#"> <i class="fa fa-mail-reply fa-lg"></i>0</a>\
               </ul>\
          </div>');
       }
     });
   } 
  |]