{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes,ScopedTypeVariables                #-}
{-# LANGUAGE TemplateHaskell            #-}
module CMS.View.Home (wArticle,wHome) where
import Yesod
import CMS.Data
import CMS.View.Common
import App.UserCenter.DB
import Data.Int
import App.Article.DB
import Data.Time.Format
import Data.Time.LocalTime
import qualified Data.Text as T
import qualified System.Random as R

convTime zone time = formatTime defaultTimeLocale "%m-%d" (utcToLocalTime zone  time)

wArticle::Maybe User -> (Int64,Article) -> [(Int64,Article)] -> Widget
wArticle mayLogin (id,article) news = do
  addScriptRemote "/static/Admin/vendor/highlight/highlight.pack.js"
  addStylesheetRemote "/static/Admin/vendor/highlight/styles/vs.css"
  addScriptRemote "/static/Admin/vendor/markdown-it/markdown-it.js"
  addScriptRemote "/static/Admin/vendor/jquery/jquery.min.js"
  setTitle $ toHtml ("HSCMS - Article" <> articleTitle article)
  timeZone <- liftIO getCurrentTimeZone
  [whamlet|
  <div .row #index_context>
   <div .col-md-9 #left_layout>
    <div .block>
      <div .block_context>
        <h3 style="text-align:center">#{articleTitle article}
        <hr>
        <textarea #srcMD style="display:none">#{preEscapedToMarkup $ articleContext article}
        <div #renderMD style="padding:0 15px 0 15px;">
   <div .col-md-3 #right_layout>
      ^{wUserCard mayLogin}
      <div .block style="margin-top:8px;">
         <div .block_title>
           <h2 .block_title_text>最热文章
         <div .block2_context>
          <ul .articleList style="padding:0px 15px 0 15px">
            $forall (id,article) <- news
             <li><a href="@{ArticleR id}">#{articleTitle article}</a><span>#{convTime timeZone (articleCreateTime article)}</span>
      ^{wFriendLink}
  |]
  toWidget $ [julius|
    window.onload = function()
    {
      var md = window.markdownit({html:true,xhtmlOut:true});
      let renderHTML = md.render($("#srcMD").val());
      $("#renderMD").html(renderHTML);
      $("pre code").each(function(i, block) {hljs.highlightBlock(block);});
    }
  |]

wHome::Maybe User -> [(Int64,Article)] -> [((Int64,ArticleCate),[(Int64,Article)])] -> Widget
wHome mayLogin news tailCA = do
 timeZone <- liftIO getCurrentTimeZone
 setTitle "HSCMS - Home"
 rNum::Int <- liftIO $ R.randomRIO (1,2)
 [whamlet|
    <div .row #index_context>
     <div .col-md-9 #left_layout>
       <div .block>
         <div .block_title>
           <h2 .block_title_text>动态
         <div .block_context>
           <div #news_left_pic style="text-align:center;display:table-cell;vertical-align: middle;">
             <img style="height:100%"　 src="/static/img/rand_0#{show rNum}.jpg">
           <div #news_right>
             <ul .articleList>
              $forall (id,article) <- news
               <li><a href="/article/#{id}">#{articleTitle article}</a><span>#{convTime timeZone (articleCreateTime article)}</span>
           <div .clear>
       <div .block style="margin-top:8px;">
         <div .block_title>
           <h2 .block_title_text>文章
         <div .block_context>
           $forall ((id,cate),articles) <- tailCA
             <div .block2>
               <div .block2_title>
                 <h3 .block2_title_text>#{articleCateName cate}
                 <span .block2_more><a href="/article/cate/#{id}">更多</a>
                 <div .clear>
               <div .block2_context>
                 <ul .articleList>
                   $forall (aid,article) <- articles
                    <li>
                     <a href="/article/#{aid}">#{articleTitle article}
                     <span>#{convTime timeZone $ articleCreateTime article}
           <div .clear>
     <div .col-md-3 #right_layout>
      ^{wUserCard mayLogin}
      <div .block style="margin-top:8px;">
         <div .block_title>
           <h2 .block_title_text>最热文章
         <div .block2_context>
          <ul .articleList style="padding:0px 15px 0 15px">
            $forall (id,article) <- news
             <li><a href="/article/#{id}">#{articleTitle article}</a><span>#{convTime timeZone (articleCreateTime article)}</span>
      ^{wFriendLink}
　|]