{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
module CMS.View.Admin.Common(adminLayout,adminJump) where
import Yesod
import CMS.Data
import App.UserCenter.DB
import Data.Maybe
import qualified Data.Text as T
import CMS.View.Common

adminJump::T.Text -> Route Master  -> Handler Html
adminJump txt url  = defaultLayout msgWidget
  where
    msgWidget  = do
      wJumpFunc
      [whamlet|
        <center>
         <h1>
           #{txt}
         <p><a href=@{url} id="link_text"></a>
      |]
      toWidget $ [julius|
         window.onload = jumpMessage("@{url}","link_text",2);
      |]


adminLayout::Widget -> Maybe User -> Handler Html
adminLayout w mayUser = defaultLayout $ do
    addStylesheetRemote "/static/Admin/vendor/bootstrap/css/bootstrap.min.css"
    addStylesheetRemote "/static/Admin/vendor/metisMenu/metisMenu.min.css"
    addStylesheetRemote "/static/Admin/css/sb-admin-2.css"
    addStylesheetRemote "/static/Admin/vendor/morrisjs/morris.css"
    addStylesheetRemote "/static/admin.css"
    addStylesheetRemote "/static/Admin/vendor/font-awesome/css/font-awesome.min.css"
    addScriptRemote "/static/Admin/vendor/jquery/jquery.min.js"
    addScriptRemote "/static/Admin/vendor/bootstrap/js/bootstrap.min.js"
    addScriptRemote "/static/Admin/vendor/metisMenu/metisMenu.js"
    addScriptRemote "/static/Admin/vendor/raphael/raphael.min.js"
    addScriptRemote "/static/Admin/vendor/morrisjs/morris.min.js"
    addScriptRemote "/static/Admin/js/sb-admin-2.js"
    [whamlet|
       <div #wrapper>
        <nav .navbar .navbar-default .navbar-static-top role="navigation" style="margin-bottom: 0">
          <div .navbar-header>
             <button .navbar-toggle type="button" data-toggle="collapse" data-target=".navbar-collapse">
               <span .sr-only>Toggle navigation
               <span .icon-bar>
               <span .icon-bar>
               <span .icon-bar>
             <a class="navbar-brand" href="/">HSSiteGroup
          <ul .nav .navbar-top-links .navbar-right>
           <li .dropdown>
             <a .dropdown-toggle data-toggle="dropdown">
              <i .fa .fa-envelope .fa-fw>
              <i .fa .fa-caret-down>
             <ul .dropdown-menu .dropdown-messages>
               <li>
                 <a href="#">
                   <div>
                    <strong>???
                    <span .pull-right .text-muted>
                     <em>昨天
                   <div>???????????
               <li .divider>
               <li>
                <a href="#">
                  <div>
                    <strong>约翰
                    <span .pull-right .text-muted>
                      <em>前天
                  <div>?????????
               <li .divider>
               <li>
                <a href="#">
                  <div>
                    <strong>约翰
                    <span .pull-right .text-muted>
                      <em>前天
                  <div>?????????
               <li .divider>
               <li>
                <a .text-center href="#">
                 <strong>ReadAllNessage
                 <i .fa .fa-angle-right>
           <li .dropdown>
            <a .dropdown-toggle data-toggle="dropdown" href="#">
              <i .fa .fa-tasks .fa-fw>
              <i .fa .fa-caret-down>
            <ul .dropdown-menu .dropdown-tasks>
             <li>
              <a href="#">
               <div>
                 <p>
                  <strong>Task 1
                 <div .progress .progress-striped .active>
                  <div .progress-bar .progress-bar-success role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
             <li .divider>
             <li>
               <a .text-center href="#">
               <strong>See All Tasks
               <i .fa .fa-angle-right>
           <li .dropdown>
              <a .dropdown-toggle data-toggle="dropdown" href="#">
               <i .fa .fa-bell .fa-fw>
               <i .fa .fa-caret-down>
              <ul .dropdown-menu .dropdown-alerts>
               <li>
                <a href="#">
                 <div>
                  <i .fa .fa-comment .fa-fw>
                  New Commnet
                  <span .pull-right .text-muted .small>4 minutes ago
               <li .divider>
               <li>
                <a .text-center href="#">
                 <strong>See All Alerts
                 <i .fa .fa-angle-right>
           <li .dropdown>
            <a .dropdown-toggle data-toggle="dropdown" href="#">
             <i .fa .fa-user .fa-fw>
             <i .fa .fa-caret-down>
            <ul .dropdown-menu .dropdown-user>
             <li>
              <a href="#">
               <i .fa .fa-user .fa-fw>
               User Profile
             <li>
              <a href="#">
               <i .fa .fa-gear .fa-fw>
               Settings
             <li .divider>
             <li>
              <a href="@{LogoutR}">
               <i .fa .fa-sign-out .fa-fw>
               Logout
          <div .navbar-default .sidebar role="navigation">
           <div .sidebar-nav .navbar-collapse>
            <ul .nav #side-menu>
             <li .sidebar-search>
              <div .input-group .custom-search-form>
               <input type="text" .form-control placeholder="Search...">
               <span .input-group-btn>
                <button .btn .btn-default type="button">
                 <i .fa .fa-search>
             <li>
              <a href="/admin/index"><i class="fa fa-dashboard fa-fw"></i> 首页
             <li>
              <a href="/admin/index">
                <i class="fa fa-dashboard fa-fw">
                用户中心
                <span .fa .arrow>
              <ul .nav .nav-second-level>
                <li><a href="/admin/user/list">用户列表</a>
             <li>
              <a href="/admin/index">
               <i class="fa fa-bar-chart-o fa-fw">
               文章
               <span .fa .arrow>
              <ul .nav .nav-second-level>
               <li><a href="@{AdminR AdminArticleCateR}">文章分类</a>
               <li><a href="@{AdminR $ AdminArticleListR 0}">文章列表</a>
               <li><a href="@{AdminR AdminArticleAddR}">新建文章</a>
             <li>
              <a href="/admin/index"><i class="fa fa-edit fa-fw"></i>小组
        <div #page-wrapper>
         ^{w}
   |]
