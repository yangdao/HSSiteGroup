{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
module CMS.View.Account (wProfile,wReigister,wLogin,wChangePwd) where
import Yesod
import CMS.Data
import CMS.View.Common
import Data.Maybe
import App.UserCenter.DB

wReigister::Widget
wReigister = do
 setTitle "注册账号"
 toWidget [whamlet|
  <div .row .context>
    <div .col-md-3>
    <div #panel .white_card .col-md-6>
      <div .card_header><h1>用户注册</h1>
      <div .regfrom>
        <form #rform action="/user/register" method="POST">
           <div .form-group>
             <label for="account">账号
             <input name="account" #account .form-control type="text" />
           <div .form-group>
             <label for="pwd">密码
             <input name="pwd" #password .form-control  type="password" />
           <div .form-group>
             <label for="sure_password">确认密码
             <input #sure_password .form-control  type="password" />
           <div .form-row>
             <div .form-group>
              <label for="vcode">验证码
              <input name="vcode" #vcode .form-control  placeholder="验证码" type="text" />
             <div .form-group>
              <label for="vimg">&nbsp;
              <img #vimg  src="/user/captcha" />
           <button onclick="submitForm()" type="button" class="btn btn-primary w100">注册
    <div .col-md-3>
  |]
 toWidget [julius|
    function getId(idStr)
    {
      return document.getElementById(idStr);
    }
    function submitForm()
    {
      var pwd = getId("password").value;
      var pwd2 = getId("sure_password").value;
      if(pwd == "" || pwd2 == "")
      {
         alert("密码不能为空");
         return;
      }
      if( pwd!= pwd2)
      {
        alert("两次密码不一致");
        return;
      }
      document.getElementById("rform").submit();
    }
 |]

wLogin::Widget
wLogin = do
   setTitle "登录"
   [whamlet|
    <div .row .context>
     <div .col-md-3>
     <div #panel .white_card .col-md-6>
        <div .card_header><h1>登录</h1>
        <div .regfrom>
         <form #lform action="/user/login" method="POST">
           <div .form-group>
              <label for="account">账号
              <input name="account" #account .form-control type="text" />
           <div .form-group>
              <label for="pwd">密码
              <input name="pwd" #password .form-control  type="password" />
           <button  type="submit" class="btn btn-primary w100">登录
     <div .col-md-3>
   |]

wChangePwd::User -> Widget
wChangePwd user = do
  setTitle "修改密码"
  [whamlet|
   <div .row style="margin:10px 0 10px 0">
     <div .col-md-3>
      <div .block>
       <div .block_context #profile_menu>
         <ul >
           <li><a href="@{UserProfileR}" >个人资料</a>
           <li><a href="@{UserChangePwdR}" class="profile_active">修改密码</a>
     <div .col-md-9>
      <div .block>
       <form style="text-align:center;margin:auto" method="POST"  action="@{UserChangePwdR}">
        <div .block_context style="padding:10px 30px 10px 20px;">
          <table style="border-spacing:20px;border-collapse:separate;margin:auto;width:90%">
            <tr>
             <th width="60px">旧密码
             <td>
              <input type="password" name="oldPwd" .form-control>
            <tr>
             <th width="60px">新密码
             <td>
              <input type="password" name="newPwd" #newPwdID  .form-control>
            <tr>
             <th width="60px">确认密码
             <td>
              <input type="password" #newPwd2ID .form-control>
            <tr>
             <td colspan="2">
              <button type="submit" style="width:100%" .btn .btn-primary>提交
  |]

wProfile::User -> Widget
wProfile user = do
  setTitle "玩家信息"
  [whamlet|
   <div .row style="margin:10px 0 10px 0">
    <div .col-md-3>
      <div .block>
       <div .block_context #profile_menu>
         <ul >
           <li><a href="@{UserProfileR}" class="profile_active">个人资料</a>
           <li><a href="@{UserChangePwdR}">修改密码</a>
    <div .col-md-9>
      <div .block>
       <form style="text-align:center;margin:auto" method="POST" enctype="multipart/form-data" action="@{UserProfileR}">
        <div .block_context style="padding:10px 30px 10px 20px;">
         <table style="border-spacing:20px;border-collapse:separate;margin:auto;width:90%">
          <tr>
           <th width="40px">头像
           <td>
            <img width="100px" src="#{userHeadImg user}">
            <input name="headImg" style="margin-top:10px" type="file" .form-control-file>
          <tr>
           <th>昵称
           <td>
            <input name="nickName" .form-control placeholder="昵称" value="#{userNickName user}">
          <tr>
           <th>签名
           <td width="400px">
            <textarea name="shortDesc" .form-control>#{userShortDesc user}
          <tr>
           <th>Email
           <td>
            <input name="email" .form-control placeholder="Email" value="#{fromMaybe "" $ userEmail user}">
          <tr>
           <th>QQ
           <td>
            <input name="qq" .form-control placeholder="QQ" value="#{fromMaybe "" $ userQq user}">
          <tr>
           <td colspan="2">
            <button type="submit" style="width:100%" .btn .btn-primary>提交
  |]

