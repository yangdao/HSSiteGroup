{-# LANGUAGE OverloadedStrings               #-}
{-# LANGUAGE QuasiQuotes,ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell              #-}
module CMS.View.Article (wArticleCateList,wArticleHome) where
import Yesod
import CMS.Data
import App.UserCenter.DB
import CMS.View.Common
import App.Article.DB
import Data.Int
import App.Article.Manager
import App.Common.Table
import App.Article.DB
import Data.Time.Format
import Data.Time.LocalTime

convTime zone time = formatTime defaultTimeLocale "%m-%d" (utcToLocalTime zone  time)

wArticleCateList::Maybe User -> (Int64,ArticleCate) -> [(Int64,Article)] -> Widget
wArticleCateList mayLogin (cid,cate) articleList = do
    let cName = articleCateName cate
    setTitle $ toHtml cName
    [whamlet|
    <div .row #index_context>
      <div .col-md-9 #left_layout>
        <div .block>
          <div .block_title>
            <h2 .block_title_text #cateListTitleText>#{articleCateName cate}
          <div .block_context #articleLst>
            <ul>
             $forall (aid,article) <- articleList
               <li><a href="@{ArticleR aid}">#{articleTitle article}</a>
      <div .col-md-3 #right_layout>
        ^{wUserCard mayLogin}
        ^{wFriendLink}
    |]

wArticleHome::Maybe (Int64,User) -> [(Int64,ArticleCate)] -> Widget
wArticleHome mayLogin cateLst = do
  setTitle "文章"
 
  [whamlet|
    <div .row #index_context>
      <div .col-md-9 #left_layout>
        $forall (id,cate) <- cateLst 
         <div .block style="margin-top:8px;">
          <div .block_title>
           <h2 .block_title_text>#{articleCateName cate}
          <div .block_context>
            ^{cate2 id}
      <div .col-md-3 #right_layout>
        ^{wUserCard $ snd <$> mayLogin}
        ^{wFriendLink}
  |]
 where
   cate2::Int64 -> Widget 
   cate2 cid = do
     cates <- liftHandler $ queryCate cid
     [whamlet|
       $forall (id,cate) <- cates
         <div .block2>
            <div .block2_title>
              <h3 .block2_title_text>#{articleCateName cate}
              <span .block2_more><a href="/article/cate/#{id}">更多</a>
              <div .clear>
            <div .block2_context>
              <ul .articleList>
                ^{wArticle $ fromIntegral id}
       <div .clear>
     |]
   wArticle::Int -> Widget
   wArticle cid = do
    timeZone <- liftIO getCurrentTimeZone
    aList::[(Int64,Article)] <- liftHandler $ queryArticleList (fst <$> mayLogin) 10 [ArticleOwnerCateId==.(fromIntegral cid)] 
    [whamlet|
      $forall (id,article) <- aList
       <li>
         <a href="@{ArticleR id}">#{articleTitle article}
         <span>#{convTime timeZone $ articleCreateTime article}
    |]