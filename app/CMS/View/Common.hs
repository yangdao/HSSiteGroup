{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes,ScopedTypeVariables                #-}
{-# LANGUAGE TemplateHaskell            #-}
module CMS.View.Common (indexLayout,wSiteCommonLink,wFriendLink,
wSiteHeader,wSiteFooter,jumpMessage,wJumpFunc,wUserCard) where
import CMS.Data
import Yesod
import qualified Data.Text as T
import Text.Julius
import App.UserCenter.DB
import Data.Maybe
import qualified System.Random as R
import Data.Int

wSiteCommonLink::Widget
wSiteCommonLink = do
    addStylesheetRemote "https://cdn.bootcss.com/twitter-bootstrap/3.3.7/css/bootstrap.min.css"
    addStylesheetRemote "/static/cms_site.css"

wSiteHeader::Maybe User -> Widget
wSiteHeader mayUser = do
 rNum::Int <- liftIO $ R.randomRIO (2,5)
 [whamlet| 
  <div #header>
    <div .container>
      <a #header_logo href="@{HomeR}">Seija
      <ul #header_nav>
       <li><a href="@{HomeR}">首页</a>
       <li><a href="https://travellings.netlify.com" target="blank" title="开往-友链接力">Travellings
       <li .clear>
      $maybe user <- mayUser
       <div #user_info>
         <a href="@{UserProfileR}">#{userNickName user}
         $if (userRoleType user) > 0
           <a href="/admin/index">后台
         $else
           <blank>
         <a href="@{LogoutR}">退出
       <div .clear>
      $nothing
       <div #user_info><a href="@{LoginR}">登录</a><a href="@{RegisterR}">注册</a>
       <div .clear>
  <div #nav_mask>
  <div #blur_bg style="background-image:url('/static/img/banner#{rNum}.jpg')">
  <div #header_img>
      <img src="/static/img/banner#{rNum}.jpg">
  <div #appnav>
      <div .container .app_navlist>
        <a href="/">首页
        <a href="/article">文章
        $if isJust mayUser
         <a href="/idea/home">想法
        <div .clear>
          
|]

wSiteFooter::Widget
wSiteFooter = [whamlet|
 <div #footer>
  <div .container .tcenter>
        <div .footer_list>
         <a href="https://travellings.netlify.com" target="blank" title="开往-友链接力"><img src="https://travellings.netlify.com/assets/logo.gif" alt="开往-友链接力" width="120">
        <p>© 2018 开源社区
|]

indexLayout::Widget -> Maybe User -> Handler Html
indexLayout w isLogin = defaultLayout [whamlet|
     ^{wSiteCommonLink}
     ^{wSiteHeader isLogin}
     <div .container>
      ^{w}
     ^{wSiteFooter}
|]

wJumpFunc::Widget
wJumpFunc = toWidget  [julius|
   function jumpMessage(jumpUrl,upElemId,waitTime = 2)
   {
      tickFunc();
      setInterval(tickFunc,1000);
      function tickFunc()
      {
        document.getElementById(upElemId).innerHTML= waitTime.toString() + "秒后返回";
        waitTime --;
        if(waitTime < 0)
        {
          window.location.href = jumpUrl;
        }
      };
   }
|]

jumpMessage::T.Text -> Route Master -> Maybe User -> Handler Html
jumpMessage txt url  = indexLayout  msgWidget
  where
    msgWidget  = do
      wJumpFunc
      [whamlet|
        <div .white_cardp20 .context .jump_card>
          #{txt}
          <p><a href=@{url} id="link_text"></a>
      |]
      toWidget $ [julius|
       window.onload = jumpMessage("@{url}","link_text",2);
      |]

wUserCard:: Maybe User -> Widget
wUserCard mayUser = 
  case mayUser of
     Just user -> wUserInfoCard user
     Nothing   -> wUserLoginCard

wUserLoginCard::Widget
wUserLoginCard = [whamlet|
  <div .block>
   <div .block_title>
    <div .block_title_text>登录
   <div .block_context>
    <form action="@{LoginR}" method="POST">
     <table style="margin:auto;border-spacing:20px;border-collapse:separate;width:100%">
      <tr>
       <td colspan="2">
        <input .form-control name="account" placeholder="账号">
      <tr>
       <td colspan="2">
        <input type="password" name="pwd" .form-control  placeholder="密码">
      <tr>
       <td align="center">
        <button type="sumbit" .btn .btn-primary style="width:100%">登录
       <td align="center"> 
        <a href="@{RegisterR}" .btn .btn-success style="width:100%">注册
|]

wUserInfoCard::User-> Widget
wUserInfoCard user = [whamlet|
<div .block>
   <div .block_context #card_user_info>
      <img #headImg src="#{T.unpack $ userHeadImg user}" width="100px" height="100px" >
      <div #card_user_right>
         <div #card_user_name>#{userNickName user}
         <div #card_user_desc>#{userShortDesc user}
      <div .clear>
|]

wFriendLink::Widget
wFriendLink =
  [whamlet|
    <div .block style="margin-top:8px;">
      <div .block_title>
        <h2 .block_title_text>链接
      <div .block2_context>
        <ul style="padding:0px 15px 0 15px">
          <li><a target="_blank" href="https://github.com/ydzz">github</a>
          <li><a href="https://gitee.com/yangdao">gitee</a>
  |]