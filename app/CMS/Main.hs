{-# LANGUAGE EmptyDataDecls,ViewPatterns             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies,InstanceSigs  #-}

module CMS.Main where
import CMS.Data
import Yesod
import CMS.Handler.Home
import CMS.Handler.Article
import CMS.Handler.Account
import CMS.Handler.Admin.AdminHome (getAdminHomeR)
import CMS.Handler.Admin.User (getAdminUserListR)
import CMS.Handler.Admin.Article
import CMS.Handler.Idea

mkYesodDispatch "Master" resourcesMaster

